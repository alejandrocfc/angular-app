import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) {}

  get_publications() {
    return this.httpClient.get('/api/publications');
  }
  get_authors() {
    return this.httpClient.get('/api/authors');
  }
}
