import { Component } from '@angular/core';
import { DataService } from './data.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {

  showMenu: boolean;
  publications: [];
  listFiltered: any;
  authors: [];
  activeAuthor: string;
  messageFilter: string;
  sortLabel: boolean;
  showMessage: boolean;

  constructor(
    private dataService: DataService
  ) {
    const publications = this.dataService.get_publications();
    const authors = this.dataService.get_authors();
    // Chain request  to fetch authors and publications
    forkJoin([publications, authors]).subscribe((results: object) => {
      if ( results[0].success && results[1].success ) {
        this.publications = results[0].data;
        this.listFiltered = results[0].data;
        this.authors = results[1].data;
      }
    });
    this.showMenu = false;
    this.sortLabel = false;
  }

  // Function to open/close side menu
  handleMenu() {
    this.showMenu = !this.showMenu;
  }

  // Function to show the publications write by author
  setAuthor(email) {
    if (this.activeAuthor === email) {
      this.listFiltered = this.publications;
      this.activeAuthor = '';
    } else {
      this.activeAuthor = email;
      this.listFiltered = this.publications.filter((item: any) => {
        return item.author.email === email;
      });
    }
  }

  // Function to sort publications oldest-newest
  sortList() {
    this.sortLabel = !this.sortLabel;
    this.listFiltered = this.publications.reverse();
  }

  // Function to handle search publications
  onSearchChange(searchValue: string): void {
    if (searchValue.length >= 3) {
      const filtered = this.publications.filter((item: any) => {
        return item.title.search(new RegExp(searchValue, 'i')) >= 0;
      });
      if (filtered.length > 0) {
        this.showMessage = false;
        this.messageFilter = '';
      } else {
        this.showMessage = true;
        this.messageFilter = 'Not publications to show';
      }
      this.listFiltered = filtered;
    }
    if (searchValue.length <= 2) {
      if (searchValue.length === 0) {
        this.showMessage = false;
        this.messageFilter = '';
        this.listFiltered = this.publications;
      } else {
        this.showMessage = true;
        this.listFiltered = this.publications;
        this.messageFilter = 'To search please enter more than 2 characters';
      }
    }
  }

}
