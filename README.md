# Sweatworks

Frontend test for Sweatworks.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.1.3.

## Requiriments

- [Angular CLI](https://github.com/angular/angular-cli) version 8.1.3. 
- Node.js version >= 6.x.x

## Installation
- Clone repo.
```
$ git clone https://github.com/alejandrocfc/angular-app
```
- Go into the project folder.
- Install dependencies
```
$ npm install
```

## Proxy configuration

On the root folder `proxy.confg.json` is a file for set proxy url to make the communication between front and back.

This file is used for scripts `package.json` 

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Scaffolding

`src` is the main folder,  where:

- `styles.scss` : Main file for styles
- `app/app.component.html` : Main html file where is showed information
- `app/app.component.ts` : Main typescript file to get logic and binding information to html file
- `app/data.service.ts` : Typescript file to handle request 
